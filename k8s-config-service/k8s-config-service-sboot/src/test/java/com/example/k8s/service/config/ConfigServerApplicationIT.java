package com.example.k8s.service.config;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * initializes a NonStopSingletonDockerContainer
 * @author ibaou
 *
 */
@Testcontainers
@ActiveProfiles("it")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = ConfigServerApplicationIT.Initializer.class)
class ConfigServerApplicationIT {

	private static final int CONSUL_HTTP_PORT = 8500;
	
	@Value("${spring.application.name}")
	private String applicationName;
	
	@Autowired
	private DiscoveryClient discoveryClient;

	/**
	 * consul: image: consul:1.6.2 ports: - 8300 - 8500 - 8600 command: consul agent
	 * -dev -ui -client 0.0.0.0
	 */
	/**
	 * 
	 */
	@Container
	public static GenericContainer<?> consul = new GenericContainer<>("consul")
			.withExposedPorts(CONSUL_HTTP_PORT)
			.waitingFor(Wait.forListeningPort());

	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
			TestPropertyValues
					.of("spring.cloud.consul.host=localhost",
							"spring.cloud.consul.port=" + consul.getMappedPort(CONSUL_HTTP_PORT))
					.applyTo(configurableApplicationContext.getEnvironment());
		}
	}

	@Test
	public void testApplicationServiceRegisteredInConsul() {
		assertThat(discoveryClient.getInstances(applicationName).isEmpty(), is(false));
	}
	
	@AfterAll
	public static void afterAll() {
	System.out.println("here");	
	}
}
