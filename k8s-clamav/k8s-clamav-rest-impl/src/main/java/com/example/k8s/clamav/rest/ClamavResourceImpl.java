package com.example.k8s.clamav.rest;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.k8s.clamav.VirusScanFile;
import com.example.k8s.clamav.VirusScanFileList;
import com.example.k8s.clamav.VirusScanFileList.VirusScanFileListBuilder;
import com.example.k8s.clamav.VirusScanResult;
import com.example.k8s.clamav.VirusScanResult.Result;
import com.example.k8s.clamav.VirusScanResult.VirusScanResultBuilder;

import lombok.extern.slf4j.Slf4j;
import xyz.capybara.clamav.ClamavClient;
import xyz.capybara.clamav.commands.scan.result.ScanResult;

/**
 * clamav.host: localhost clamav.port: 3310 clamav.timeout: 100
 * 
 * @author ibaou
 *
 */
@Slf4j
@RestController
public class ClamavResourceImpl implements ClamavResource {

	@Value("${clamav.host:localhost}")
	private String hostname;
	@Value("${clamav.timeout:100}")
	private Short timeout;
	@Value("${clamav.port:3310}")
	private Integer port;

	@Override
	public boolean ping() {
		new ClamavClient(hostname, port).ping();
		return true;
	}

	@Override
	public VirusScanResult scan(MultipartFile[] files) {
		VirusScanResultBuilder scanResultBuilder = VirusScanResult.builder();
		scanResultBuilder.result(Result.OK);
		try {
			ClamavClient client = new ClamavClient(hostname, port);
			for (MultipartFile file : files) {
				log.debug("scanning file: {} of size: {}", file.getOriginalFilename(), file.getSize());
				final ScanResult scanResult = client.scan(file.getInputStream());				
				if( scanResult instanceof ScanResult.VirusFound ) {
					VirusScanFileListBuilder scanFileListBuilder = VirusScanFileList.builder();					
					scanResultBuilder.result(Result.VIRUS_FOUND);
					scanFileListBuilder.fileName(file.getOriginalFilename());
					final Map<String, Collection<String>> viruses = ((ScanResult.VirusFound) scanResult).getFoundViruses();
				    log.debug("scan result for file: {} viruses: {}", file.getOriginalFilename(), viruses);
				    for( String fileAlias : viruses.keySet() ) {
				    	scanFileListBuilder.scanFile( VirusScanFile.builder().fileAlias(fileAlias).viruses(viruses.get(fileAlias)).build() );
				    }
				    scanResultBuilder.infectedFile(scanFileListBuilder.build());
				}
			}
		} catch (IOException ioe) {
			log.error("error.clamav.ping", ioe);
		}
		return scanResultBuilder.build();
	}

}
