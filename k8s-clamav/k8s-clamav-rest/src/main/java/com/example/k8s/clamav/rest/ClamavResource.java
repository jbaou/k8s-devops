package com.example.k8s.clamav.rest;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.example.k8s.clamav.VirusScanResult;

import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;

@FeignClient(name = "k8s-clamav-sboot", configuration = ClamavResource.MultipartSupportConfig.class)
@RequestMapping("/clamav")
public interface ClamavResource {

	/**
	 * inner class to configure feign multipart support
	 * @author ibaou
	 *
	 */
	public class MultipartSupportConfig {

		@Autowired
		private ObjectFactory<HttpMessageConverters> messageConverters;

		@Bean
		public Encoder feignFormEncoder() {
			return new SpringFormEncoder(new SpringEncoder(messageConverters));
		}		
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public boolean ping();

	@PostMapping(consumes = { MediaType.MULTIPART_FORM_DATA_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	VirusScanResult scan(@RequestPart(name = "file", required = true) MultipartFile[] file);

}
