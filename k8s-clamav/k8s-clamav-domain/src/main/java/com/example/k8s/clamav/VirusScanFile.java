package com.example.k8s.clamav;

import java.io.Serializable;
import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class VirusScanFile implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String fileAlias;
	
	@Singular
	private Collection<String> viruses;
	
}
