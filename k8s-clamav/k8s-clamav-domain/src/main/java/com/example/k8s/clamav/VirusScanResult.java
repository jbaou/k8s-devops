package com.example.k8s.clamav;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class VirusScanResult implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public static enum Result { OK, VIRUS_FOUND };
	
	private Result result;
	
	@Singular
	private List<VirusScanFileList> infectedFiles;
	
	

}
