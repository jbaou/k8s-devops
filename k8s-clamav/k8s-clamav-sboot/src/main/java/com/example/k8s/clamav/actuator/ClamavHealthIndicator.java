package com.example.k8s.clamav.actuator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import com.example.k8s.clamav.rest.ClamavResource;

/**
 * calls the <code>ClamavResource</code> ping method to determine if clamav is available for scanning
 * 
 * @author ibaou
 *
 */
@Component
public class ClamavHealthIndicator implements HealthIndicator {

	@Autowired

	ClamavResource clamResourceClient;

	@Override
	public Health health() {
		final boolean result = clamResourceClient.ping();
		if (!result) {
			return Health.down().withDetail("clamav ping:", result).build();
		}
		return Health.up().build();
	}
}