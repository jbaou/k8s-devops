DO
$do$
BEGIN
   IF EXISTS (SELECT FROM pg_database WHERE datname = 'service_one') THEN
      RAISE NOTICE 'Database already exists';  -- optional
   ELSE
      PERFORM dblink_exec('dbname=' || current_database()  -- current db
                        , 'CREATE DATABASE service_one OWNER service_one');
   END IF;
END
$do$;