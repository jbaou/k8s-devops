create extension dblink;

DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT 
      FROM   pg_catalog.pg_roles
      WHERE  rolname = 'service_one') THEN

      CREATE ROLE service_one LOGIN PASSWORD 'service_one';
   END IF;
END
$do$;