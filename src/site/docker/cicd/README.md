# CI/CD docker compose
* postgres:12.1 (port 5432 - internal)
* pgadmin:latest (port 5050 - external)
* jenkinsci/blueocean:1.21.0 (port 8080/50000 - external )
* sonarqube:7.9.2-community (port 9000/9092 - external)

### From this directory(k8s-parent\src\site\docker)
it is advisable to introduce a '.env' file to specify at least the root data directory of this compose stack, f.e:
```DATA_ROOT=D:/Kubernetes/docker-data/cicd```
if not, all data will be stored relative to this path ./data/cicd (inside eclipse it is a pain ... )

#Note:
sonarqube depends on postrges;
Start postgres first during first installation to ensure tables exist
TODO: wait for PG to create tables before start

### run to start (FIRST TIME EVER) and give it a minute ...:
```sh
$ docker-compose up -d postgres
```

### run to start (ANYTIME AFTER THE FIRST):
```sh
$ docker-compose up (-d)
```

### run for logs:
```sh
$ docker-compose logs -f
```

### run to stop (DATA IS PRESERVED):
```sh
$ docker-compose stop
```

### run to restart a specific container:
```sh
$ docker-compose restart sonar
```


### run to stop and remove volumes / network (ALL DATA WILL BE LOST):
```sh
$ docker-compose down -v --remove-orphans
```
