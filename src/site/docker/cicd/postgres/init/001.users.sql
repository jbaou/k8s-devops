create extension dblink;

DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT 
      FROM   pg_catalog.pg_roles
      WHERE  rolname = 'sonarqube') THEN

      CREATE ROLE sonarqube LOGIN PASSWORD 'sonarqube';
   END IF;
END
$do$;