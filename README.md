see respective readmes ... :)

quick summary (you have to have my credentials and be logged on to the hub ... )

* mvn clean compile jib:build
* docker run -idt -p8080:8080 jbaou/k8s-service-one-sboot

or use your own credentials and do this:

* mvn clean compile jib:build -Dimage=your_registry_url_port/your_profile/pick_a_name 
* docker run -idt -p8080:8080 your_profile/pick_a_name

ps. I am sure the devops would not like you to have their registry's write credentials :)
