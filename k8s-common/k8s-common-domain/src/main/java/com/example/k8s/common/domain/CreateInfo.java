package com.example.k8s.common.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Value;

/**
 * Creation related persistence
 * @author ibaou
 *
 */
@Value
public class CreateInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date dateCreated = new Date();
    
    private String createdById;

    public Date getDateCreated() {
        return dateCreated;
    }

    public String getCreatedById() {
        return createdById;
    }

}
