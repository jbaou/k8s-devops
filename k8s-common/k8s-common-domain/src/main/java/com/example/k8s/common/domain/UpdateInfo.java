package com.example.k8s.common.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Value;

/**
 * Update related persistence
 * 
 * @author ibaou
 *
 */
@Value
public class UpdateInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date dateUpdated;

    private String updatedById;
}
