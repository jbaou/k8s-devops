package com.example.k8s.common.domain;

import java.io.Serializable;

/**
 *  
 * @author ibaou
 *
 */
public interface Identifiable<T extends Serializable> {
        
    /**
     * @return the Id
     */
    T getId();

}
