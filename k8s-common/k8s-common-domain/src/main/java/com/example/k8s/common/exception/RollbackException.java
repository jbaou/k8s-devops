package com.example.k8s.common.exception;

/**
 * Throw when you want to rollback
 * @author ibaou
 *
 */
public class RollbackException extends RuntimeException{

	private static final long serialVersionUID = 1L;

    public RollbackException(Throwable cause) {
        super(cause);
    }	
}
