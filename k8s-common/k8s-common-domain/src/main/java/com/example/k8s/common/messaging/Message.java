package com.example.k8s.common.messaging;

import java.io.Serializable;

import com.example.k8s.common.domain.Identifiable;

public interface Message<T extends Serializable> extends Serializable, Identifiable<String> {

	T getPayload();

}
