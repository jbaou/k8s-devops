package com.example.k8s.common.domain;

import java.io.Serializable;

import lombok.Value;

@Value
public class Extra implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String value;

}
