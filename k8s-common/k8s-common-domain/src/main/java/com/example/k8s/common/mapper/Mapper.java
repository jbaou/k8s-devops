package com.example.k8s.common.mapper;

public interface Mapper<D, E> {	
	D toDomain(E entity);
	E toEntity(D domain);	    
}
