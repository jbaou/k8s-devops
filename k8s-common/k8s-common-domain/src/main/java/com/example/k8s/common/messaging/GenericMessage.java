package com.example.k8s.common.messaging;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@ToString
@NoArgsConstructor
@Getter
@SuperBuilder
@JsonTypeInfo(use=Id.CLASS, include=As.PROPERTY, property="@class")
public class GenericMessage<T extends Serializable> implements Message<T> {
	
	private static final long serialVersionUID = 1L;

	@Builder.Default
	private String id = UUID.randomUUID().toString();
	
	private String relatedMessageId;
	
	@JsonTypeInfo(use=Id.CLASS, include=As.PROPERTY, property="@class")
	private T payload;

}
