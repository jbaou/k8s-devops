package com.example.k8s.common.domain;

import java.util.Set;

/**
 * Historyable objects ( objects that have history )
 * 
 * @author ibaou
 *
 */
public interface Historyable<T extends History<?>> {
    
    String getCurrentAction();
        
    /**
     * @return all histories
     */
    Set<T> getHistories();
    
    /**
     * Create a history object
     * @param args
     * @return
     */
    T createHistory(Object... args);
    
}
