package com.example.k8s.common.domain;

/**
 * Deletable objects
 * @author ibaou
 *
 */
public interface Deletable {
        
    DeleteInfo getDeleteInfo();

}
