package com.example.k8s.common.exception;

/**
 * Throw when an entity was not found
 * @author ibaou
 *
 */
public class EntityNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;

}
