package com.example.k8s.common.domain;

/**
 * History objects ( for Historyable objects )
 * 
 * @author ibaou
 *
 */
public interface History<T extends Historyable<?>> {
        
    /**
     * @return
     */
    T getCurrent();
    
    void setAction(String action);
    
}
