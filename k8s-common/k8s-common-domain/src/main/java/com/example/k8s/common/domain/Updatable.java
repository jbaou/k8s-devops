package com.example.k8s.common.domain;

/**
 * Updatable objects
 * @author ibaou
 *
 */
public interface Updatable {
        
    UpdateInfo getUpdateInfo();
}
