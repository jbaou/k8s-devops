package com.example.k8s.common.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Value;

/**
 * Deletion related persistence
 * 
 * @author ibaou
 *
 */
@Value
public class DeleteInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date dateDeleted;
    
    private String deletedById;
    
    private Boolean deleted;
        
}
