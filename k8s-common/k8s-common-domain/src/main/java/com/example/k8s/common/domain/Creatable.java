package com.example.k8s.common.domain;

/**
 * Creatable objects ( trace date and principal creating an object )
 * 
 * @author ibaou
 *
 */
public interface Creatable {
        
	CreateInfo getCreateInfo();
}
