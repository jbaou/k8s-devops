package com.example.k8s.common.validation;

/**
 * Hold multiple common interfaces for grouping validation via <code>Groups</code>
 * @author ibaou
 *
 */
public interface Actions {
	
	public interface Create {}
	public interface Edit {}
	public interface Copy {}
	public interface Review {}
}


