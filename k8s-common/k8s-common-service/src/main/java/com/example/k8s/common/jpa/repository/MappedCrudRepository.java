package com.example.k8s.common.jpa.repository;

import java.util.Optional;

public interface MappedCrudRepository<D, I> {
	
	D save(D domain);

	Iterable<D> saveAll(Iterable<D> domains);

	Optional<D> findById(I id);

	boolean existsById(I id);

	Iterable<D> findAll();

	Iterable<D> findAllById(Iterable<I> ids);

	long count();

	void deleteById(I id);

	void delete(D domain);

	void deleteAll(Iterable<? extends D> domains);

	void deleteAll();	


}
