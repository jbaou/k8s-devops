package com.example.k8s.common.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Information about who/when locked (checked-in) the entity
 * @author ibaou
 *
 */
@Embeddable
public class LockInfoEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_LOCKED", nullable=true)
    private Date dateLocked;
    
    @Column(name = "F_LOCKED_BY", nullable=true)
    private String lockedById;
    
    @Column(name = "IS_LOCKED", nullable=true)
    private Boolean locked;

    
    public Date getDateLocked() {
        return dateLocked;
    }

    
    public void setDateLocked(Date dateLocked) {
        this.dateLocked = dateLocked;
    }

    
    public String getLockedById() {
        return lockedById;
    }

    
    public void setLockedById(String lockedById) {
        this.lockedById = lockedById;
    }

    
    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }
}
