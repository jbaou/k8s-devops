package com.example.k8s.common.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Update related persistence
 * 
 * @author ibaou
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Embeddable
public class UpdateInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_UPDATED")
    private Date dateUpdated;

    @Column(name = "F_UPDATED_BY")
    private String updatedById;

}
