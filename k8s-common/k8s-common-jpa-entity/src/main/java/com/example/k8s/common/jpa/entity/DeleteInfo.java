package com.example.k8s.common.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Deletion related persistence
 * 
 * @author ibaou
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Embeddable
public class DeleteInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_DELETED")
    private Date dateDeleted;
    
    @Column(name = "F_DELETED_BY")
    private String deletedById;
    
    @Column(name = "IS_DELETED")
    private Boolean deleted;
}
