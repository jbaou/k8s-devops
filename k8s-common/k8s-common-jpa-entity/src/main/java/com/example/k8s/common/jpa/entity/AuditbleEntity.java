package com.example.k8s.common.jpa.entity;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@MappedSuperclass
// bug for jpa events inside @Embeddable when included in @MappedSuperclass: https://hibernate.atlassian.net/browse/HHH-13316
public abstract class AuditbleEntity implements Serializable {

	private static final long serialVersionUID = 1L;
            
    @Embedded
    private CreateInfo createInfo = new CreateInfo();
    
    @Embedded
    private UpdateInfo updateInfo = new UpdateInfo();
    
    @Embedded
    private DeleteInfo deleteInfo= new DeleteInfo();  
    
}
