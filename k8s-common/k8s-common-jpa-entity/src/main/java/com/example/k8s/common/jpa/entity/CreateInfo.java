package com.example.k8s.common.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Creation related persistence
 * @author ibaou
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Embeddable
public class CreateInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_CREATED", nullable=true)
    private Date dateCreated;
    
    @Column(name = "F_CREATED_BY", nullable=true)
    private String createdById;

}
