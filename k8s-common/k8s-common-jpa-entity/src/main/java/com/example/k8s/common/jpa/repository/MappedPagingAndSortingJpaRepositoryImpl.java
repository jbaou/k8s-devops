package com.example.k8s.common.jpa.repository;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.k8s.common.mapper.Mapper;

@Service
@SuppressWarnings("unused")
@Transactional(readOnly = true)
public class MappedPagingAndSortingJpaRepositoryImpl<D, T, I> implements MappedCrudRepository<D, I>{
	
	private final Mapper<D, T> mapper;
	private final SimpleJpaRepository<T, I> springJpaRepo;
	
	private final Class<D> domainClass;
	private final Class<T> entityClass;
	
	@SuppressWarnings("unchecked")
	public MappedPagingAndSortingJpaRepositoryImpl(@Autowired EntityManager em, @Autowired Mapper<D, T> mapper) {
        this.domainClass = (Class<D>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
        
        this.entityClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[1];       
        
        this.springJpaRepo = new SimpleJpaRepository<>(entityClass, em);
        this.mapper = mapper;
	}
	
	@Override
	@Transactional
	public D save(D domain) {
		return mapper.toDomain( springJpaRepo.save( mapper.toEntity(domain)) );
	}
	
	@Override
	@Transactional
	public Iterable<D> saveAll(Iterable<D> domains) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public boolean existsById(I id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<D> findAll() {
		return StreamSupport.stream(springJpaRepo.findAll().spliterator(), false ).map(e ->  (D)mapper.toDomain(e) ).collect(Collectors.toList());
	}

	@Override
	public List<D> findAllById(Iterable<I> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	@Transactional
	public void deleteById(I id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public void delete(D entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public void deleteAll(Iterable<? extends D> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Optional<D> findById(I id) {		
		return Optional.ofNullable(mapper.toDomain(springJpaRepo.findById(id).orElse(null)));
	}

}
