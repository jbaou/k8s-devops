package com.example.k8s.common.utils;

/**
 * String Utilities (just to have something to unit test)
 * 
 * @author ibaou
 *
 */
public final class StringUtils {

	private StringUtils() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * check id a string is null or trimmed matches the empty string.
	 * 
	 * @param input
	 * @return
	 */
	public static boolean isEmptyOrNull(final String input) {
		return input == null || "".equals(input.trim());
	}

}
