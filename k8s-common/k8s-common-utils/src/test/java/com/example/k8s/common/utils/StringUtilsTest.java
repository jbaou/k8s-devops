package com.example.k8s.common.utils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.junit.jupiter.api.Test;

public class StringUtilsTest {
	
	
	@Test
	public void testIsUtilClass() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	    Constructor<StringUtils> constructor = StringUtils.class.getDeclaredConstructor();
	    assertTrue(!constructor.canAccess(null));
	    
	    constructor.setAccessible(true);
        Throwable thrown = assertThrows(InvocationTargetException.class, () -> constructor.newInstance());
        assertThat(thrown.getCause(), instanceOf(IllegalStateException.class));	    	    
        assertThat(thrown.getCause().getMessage(), is("Utility class"));
	    constructor.setAccessible(false);
	}
	
	@Test
	public void testIsEmptyOrNull() {
		assertTrue(StringUtils.isEmptyOrNull(null));
		assertTrue(StringUtils.isEmptyOrNull(""));
		assertTrue(StringUtils.isEmptyOrNull("    "));
		assertTrue(!StringUtils.isEmptyOrNull("hello"));
		assertTrue(!StringUtils.isEmptyOrNull(" h ell o "));
	}

}
