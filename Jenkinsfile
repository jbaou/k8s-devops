pipeline {
  agent {
    docker {
      image 'maven:3.6.3-jdk-13'
      args '''-v $HOME/.m2:/root/.m2:z -u root
-v /var/run/docker.sock:/var/run/docker.sock 
-v ${PWD}:/usr/src/app -w /usr/src/app
--network cicd_default
--privileged'''
    }

  }
  stages {
    stage('Initialize') {
      steps {
        sh 'mvn clean'
      }
    }

    stage('Build') {
      steps {
        sh 'mvn compile -Dmaven.test.skip=true'
      }
    }

    stage('Unit Tests') {
      steps {
        sh 'mvn test -Dmaven.test.failure.ignore=true'
      }
    }

    stage('Quality') {
      parallel {
        stage('Sonar') {
          steps {
            withSonarQubeEnv('sonarqube') {
              sh 'mvn sonar:sonar -Dlicense.skip=true'
            }

          }
        }

        stage('IT Tests') {
          steps {
            sh 'mvn package failsafe:integration-test -Dmaven.test.failure.ignore=true'
          }
        }

      }
    }

    stage('QA') {
      parallel {
        stage('Sonar Gate') {
          steps {
            timeout(time: 2, unit: 'MINUTES') {
              waitForQualityGate(webhookSecretId: 'k8s-parent-webhook', abortPipeline: true)
            }

          }
        }

        stage('Collect Tests') {
          steps {
            junit(testResults: '**/target/surefire-reports/**/*.xml,**/target/failsafe-reports/**/*.xml', healthScaleFactor: 10)
          }
        }

      }
    }

    stage('Publish') {
      parallel {
        stage('Jar Publish') {
          steps {
            sh '''echo "When I have nexus"
echo "mvn deploy:deploy -Dmaven.test.skip=true"'''
          }
        }

        stage('Docker Publish') {
          steps {
            sh '''mvn jib:build \\
-pl :k8s-config-service-sboot,:k8s-service-one-sboot \\
-DallowInsecureRegistries=true \\
-Djib.to.auth.username=${DOCKER_REGISTRY_CREDS_USR} \\
-Djib.to.auth.password=${DOCKER_REGISTRY_CREDS_PSW}'''
          }
        }

      }
    }

    stage('Notify') {
      steps {
        echo 'SUCCESS'
      }
    }

  }
  environment {
    DOCKER_REGISTRY_CREDS = credentials('docker-registry')
  }
}