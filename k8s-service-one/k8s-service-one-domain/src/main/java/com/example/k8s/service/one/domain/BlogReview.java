package com.example.k8s.service.one.domain;

import java.io.Serializable;
import java.util.UUID;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.example.k8s.common.domain.Creatable;
import com.example.k8s.common.domain.CreateInfo;
import com.example.k8s.common.domain.Identifiable;
import com.example.k8s.common.domain.Updatable;
import com.example.k8s.common.domain.UpdateInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonTypeInfo(use=Id.CLASS, include=As.PROPERTY, property="@class")
public class BlogReview implements Serializable, Creatable, Updatable, Identifiable<Serializable>{
	
	private static final long serialVersionUID = 1L;

	private String uuid;
	
	@NotEmpty
	private String blogUuid;
	
	@NotEmpty
    private String comment;
    
    private CreateInfo createInfo;
    private UpdateInfo updateInfo;
    
    @NotNull
    private boolean active;

	@Override
	public Serializable getId() {
		return uuid;
	}

	public BlogReview init() {
		setUuid(UUID.randomUUID().toString());
		return this;
	}
	
}
