package com.example.k8s.service.one.domain;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.example.k8s.common.domain.Creatable;
import com.example.k8s.common.domain.CreateInfo;
import com.example.k8s.common.domain.Deletable;
import com.example.k8s.common.domain.DeleteInfo;
import com.example.k8s.common.domain.Identifiable;
import com.example.k8s.common.domain.Updatable;
import com.example.k8s.common.domain.UpdateInfo;
import com.example.k8s.common.validation.Actions.Edit;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonTypeInfo(use=Id.CLASS, include=As.PROPERTY, property="@class")
public class Blog implements Serializable, Creatable, Updatable, Deletable, Identifiable<Serializable>{
	
	private static final long serialVersionUID = 1L;
	
	public static enum Status { DRAFT, PENDING_REVIEW, REJECTED, PUBLISHED, REVOKED };

	@NotNull(groups = {Edit.class})
	private String uuid;
	
	@NotEmpty
	private String title;
	@NotEmpty
    private String content;
    
    private CreateInfo createInfo;
    private UpdateInfo updateInfo;
    private DeleteInfo deleteInfo;
    
    @NotNull
    private Status status;
    
    private Set<BlogReview> reviews;

	public void updateFrom(Blog domain) {
    	setStatus(domain.getStatus());
    	setContent(domain.getContent());
    	setTitle(domain.getTitle());    	
    	setUpdateInfo(domain.getUpdateInfo());
	}

	@Override
	public Serializable getId() {
		return uuid;
	}

	public Blog init() {
		setUuid(UUID.randomUUID().toString());
		return this;
	}
	
}
