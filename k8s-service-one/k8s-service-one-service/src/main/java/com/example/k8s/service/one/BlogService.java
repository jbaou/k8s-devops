package com.example.k8s.service.one;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.example.k8s.common.exception.EntityNotFoundException;
import com.example.k8s.service.one.domain.Blog;
import com.example.k8s.service.one.domain.BlogReview;


public interface BlogService<D extends Blog> {
	    
	/**
	 * no caching
	 * 
	 * @return list of blogs
	 */
	List<D> findAll();
    
	/**
	 * @param d the blog to create
	 * @param saveToCache - store in cache or not
	 * @return
	 */
	D create( D d, boolean saveToCache );

	/**
	 * @param the uuid of the blog to edit
	 * @param d the blog to edit
	 * @param saveToCache - store in cache or not
	 * @return
	 * @throws EntityNotFoundException 
	 * @throws Exception
	 */
	D edit(Serializable uuid, D d, boolean saveToCache) throws EntityNotFoundException;

    /**
     * no caching
     * @param l list of blogs
     */
    void createAll(@NotNull List<D> l);

	/**
	 * @param uuid
	 * @param fromCache - get from cache or fresh
	 * @return
	 */
	D find(Serializable uuid, boolean fromCache) throws EntityNotFoundException;

	/**
	 * evict a blog from cache
	 * @param uuid
	 */
	void evict(Serializable uuid);

	Serializable submitBlogForReview(@NotNull Serializable uuid);

	Serializable saveBlogFile(@NotNull Serializable uuid, byte[] bytes);

	Serializable reviewBlog(@NotNull Serializable uuid, BlogReview blogReview);

}
