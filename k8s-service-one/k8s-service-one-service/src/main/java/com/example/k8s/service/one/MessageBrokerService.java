package com.example.k8s.service.one;

import com.example.k8s.common.messaging.Message;

/**
 * Send and Receive Messages to the broker
 * @author ibaou
 *
 */
public interface MessageBrokerService {

	public <T extends Message<?>> void sendGlobalMessage(T message);
	
	/**
	 * receive and handle global application messages
	 * 
	 * @param <T>
	 * @param message
	 */
	public <T extends Message<?>> void consumeGlobalMessage(T message);

}
