package com.example.k8s.service.one;

import java.io.Serializable;

import com.example.k8s.common.jpa.repository.MappedCrudRepository;
import com.example.k8s.service.one.domain.Blog;


public interface BlogRepository<D extends Blog> extends MappedCrudRepository<D, Serializable>{
            
}
