package com.example.k8s.service.one.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.example.k8s.common.exception.EntityNotFoundException;
import com.example.k8s.service.one.BlogService;
import com.example.k8s.service.one.domain.Blog;
import com.example.k8s.service.one.domain.Blog.Status;
import com.example.k8s.service.one.rest.request.BlogRequest;
import com.example.k8s.service.one.rest.request.BlogRequest.Type;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = { BlogResource.class })
public class BlogResourceTest {

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private MockMvc mvc;

	@MockBean
	private BlogService<Blog> blogService;

	@Test
	@Execution(ExecutionMode.CONCURRENT)
	public void whenAskForBlogs_thenReturn2_status200() throws Exception {

		// in mock
		Mockito.when(blogService.findAll())
				.thenReturn(List.of(
						Blog.builder().uuid("xxx").title("title #1").content("content").status(Status.DRAFT).build(),
						Blog.builder().uuid("yyy").title("title #2").content("content").status(Status.DRAFT).build()));

		mvc.perform(get("/blogs").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].uuid").value("xxx")).andExpect(jsonPath("$[1].uuid").value("yyy"));

	}

	@Test
	public void whenAskForMissingBlog_thenReturn0_status404() throws Exception {
		// in mock
		Mockito.when(blogService.find("yyy", true)).thenThrow(EntityNotFoundException.class);
		mvc.perform(get("/blogs/yyy").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	@Execution(ExecutionMode.CONCURRENT)
	public void whenAskForExistingBlog_thenReturn1_status200() throws Exception {
		// in mock
		Mockito.when(blogService.find("xxx", true)).thenReturn(
				Blog.builder().uuid("xxx").title("title #1").content("content").status(Status.DRAFT).build());

		mvc.perform(get("/blogs/xxx").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.uuid").value("xxx"));
	}

	@Test
	public void whenCreateInvalidRequest_thenReturn1_status400() throws Exception {
		// in mock
		final BlogRequest emptyTypeBlogRequest = BlogRequest.builder().build();

		// don't bother mocking ...
		mvc.perform(
				post("/blogs/").content(mapper.writeValueAsString(emptyTypeBlogRequest)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
		
		// in mock
		final BlogRequest emptyBlogRequest = BlogRequest.builder().type(Type.CREATE).build();

		// don't bother mocking ...
		mvc.perform(
				post("/blogs/").content(mapper.writeValueAsString(emptyBlogRequest)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void whenCreateExistingBlog_thenReturn0_status400() throws Exception {
		// in mock
		final BlogRequest blogRequest = BlogRequest.builder().type(Type.CREATE)
				.blog(Blog.builder().uuid("xxx").title("title").content("content").build()).build();

		// in mock
		Mockito.when(blogService.create(blogRequest.getBlog(), true)).thenThrow(ConstraintViolationException.class);
		
		mvc.perform(
				post("/blogs/").content(mapper.writeValueAsString(blogRequest)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}	

	@Test
	public void whenCreateValidBlog_thenReturn1_status201() throws Exception {
		// in mock
		final BlogRequest blogRequest = BlogRequest.builder().type(Type.CREATE)
				.blog(Blog.builder().title("title #1").content("content").status(Status.DRAFT).build()).build();

		Mockito.when(blogService.create(blogRequest.getBlog(), true)).thenReturn(
				Blog.builder().uuid("xxx").title("title #1").content("content").status(Status.DRAFT).build());

		mvc.perform(
				post("/blogs/").content(mapper.writeValueAsString(blogRequest)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated()).andExpect(jsonPath("$.uuid").value("xxx"));
	}
	
	
	@Test
	public void whenEditInvalidRequest_thenReturn1_status400() throws Exception {
		// in mock
		final BlogRequest emptyTypeBlogRequest = BlogRequest.builder().build();

		// don't bother mocking ...
		mvc.perform(
				put("/blogs/xxx").content(mapper.writeValueAsString(emptyTypeBlogRequest)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
		
		// in mock
		final BlogRequest emptyBlogRequest = BlogRequest.builder().type(Type.EDIT).build();

		// don't bother mocking ...
		mvc.perform(
				put("/blogs/xxx").content(mapper.writeValueAsString(emptyBlogRequest)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void whenEditValidBlog_thenReturn1_status201() throws Exception {
		// in mock
		final BlogRequest blogRequest = BlogRequest.builder().type(Type.EDIT)
				.blog(Blog.builder().uuid("xxx").title("title #1").content("content").status(Status.DRAFT).build()).build();

		Mockito.when(blogService.edit("xxx", blogRequest.getBlog(), true)).thenReturn(
				Blog.builder().uuid("xxx").title("title #1").content("content").status(Status.DRAFT).build());

		mvc.perform(
				put("/blogs/xxx").content(mapper.writeValueAsString(blogRequest)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.uuid").value("xxx"));
	}
	
	@Test
	public void whenEditMissingBlog_thenReturn0_status400() throws Exception {
		// in mock
		final BlogRequest blogRequest = BlogRequest.builder().type(Type.EDIT)
				.blog(Blog.builder().uuid("yyy").title("title #1").content("content").status(Status.DRAFT).build()).build();

		Mockito.when(blogService.edit("yyy", blogRequest.getBlog(), true)).thenThrow(EntityNotFoundException.class);

		mvc.perform(
				put("/blogs/yyy").content(mapper.writeValueAsString(blogRequest)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}
}
