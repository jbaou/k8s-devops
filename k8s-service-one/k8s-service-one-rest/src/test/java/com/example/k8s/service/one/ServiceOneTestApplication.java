package com.example.k8s.service.one;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
class ServiceOneTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceOneTestApplication.class, args);
	}

}