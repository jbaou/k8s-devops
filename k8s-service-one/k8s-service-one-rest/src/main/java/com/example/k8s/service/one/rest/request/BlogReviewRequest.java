package com.example.k8s.service.one.rest.request;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.example.k8s.service.one.domain.BlogReview;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class BlogReviewRequest implements Serializable{

	private static final long serialVersionUID = 1L;
		
	public enum Type { APPROVE, REJECT }
	
	@NotNull
	private Type type;
	
	@NotNull @Valid
	private BlogReview blogReview;
}
