package com.example.k8s.service.one.rest.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ValidationErrorResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Violation> violations = new ArrayList<>();

}
