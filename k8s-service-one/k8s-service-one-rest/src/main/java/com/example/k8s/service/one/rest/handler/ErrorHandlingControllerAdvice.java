package com.example.k8s.service.one.rest.handler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.k8s.common.exception.EntityNotFoundException;
import com.example.k8s.service.one.rest.response.ValidationErrorResponse;
import com.example.k8s.service.one.rest.response.Violation;

@ControllerAdvice
class ErrorHandlingControllerAdvice {

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	ValidationErrorResponse onConstraintValidationException(ConstraintViolationException e) {
		ValidationErrorResponse error = new ValidationErrorResponse();
		if (e.getConstraintViolations() != null) {
			for (ConstraintViolation<?> violation : e.getConstraintViolations()) {
				error.getViolations()
						.add(new Violation(violation.getPropertyPath().toString(), violation.getMessage(), null));
			}
		}
		return error;
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	ValidationErrorResponse onMethodArgumentNotValidException(MethodArgumentNotValidException e) {
		ValidationErrorResponse error = new ValidationErrorResponse();
		if (e.getBindingResult().getFieldErrors() != null) {
			for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
				error.getViolations().add(new Violation(fieldError.getField(), fieldError.getDefaultMessage(), fieldError.getArguments()));
			}
		}
		return error;
	}

	@ExceptionHandler(EntityNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	void onEntityNotFoundException(EntityNotFoundException e) {
		// no body
	}

}