package com.example.k8s.service.one.rest.request;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.example.k8s.service.one.domain.Blog;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class BlogRequest implements Serializable{

	private static final long serialVersionUID = 1L;
		
	public enum Type { CREATE, EDIT, REVIEW }
	
	@NotNull
	private Type type;
	
	@NotNull @Valid
	private Blog blog;
}
