package com.example.k8s.service.one.rest.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Violation implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private final String fieldName;

	private final String message;
	
	private final Object[] args;

}