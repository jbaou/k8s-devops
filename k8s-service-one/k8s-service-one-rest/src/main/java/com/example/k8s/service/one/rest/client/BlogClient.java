package com.example.k8s.service.one.rest.client;

import java.io.Serializable;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.k8s.service.one.domain.Blog;

@FeignClient("k8s-service-one-sboot")
public interface BlogClient {
	
	@GetMapping(path = "/blogs", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Blog> getBlogs();

	@GetMapping(path = "/blogs/{id}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public Blog getBlog(@PathVariable(name = "id") Serializable id);

}
