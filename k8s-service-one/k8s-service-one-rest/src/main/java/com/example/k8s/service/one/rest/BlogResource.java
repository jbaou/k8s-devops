package com.example.k8s.service.one.rest;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.k8s.clamav.VirusScanResult;
import com.example.k8s.clamav.VirusScanResult.Result;
import com.example.k8s.clamav.rest.ClamavResource;
import com.example.k8s.common.exception.EntityNotFoundException;
import com.example.k8s.common.validation.Actions.Create;
import com.example.k8s.common.validation.Actions.Edit;
import com.example.k8s.common.validation.Actions.Review;
import com.example.k8s.service.one.BlogService;
import com.example.k8s.service.one.domain.Blog;
import com.example.k8s.service.one.rest.request.BlogRequest;
import com.example.k8s.service.one.rest.request.BlogReviewRequest;

@Validated
@RestController
@RequestMapping("/blogs")
public class BlogResource {

	@Autowired
	private BlogService<Blog> blogService;

	@Autowired
	ClamavResource clamRestClient;

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Blog> getBlogs() {
		return blogService.findAll();
	}

	@Validated(Create.class)
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public Blog createBlog(@Valid @RequestBody BlogRequest blogRequest) {
		return blogService.create(blogRequest.getBlog(), true);
	}

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Blog getBlog(@PathVariable(name = "id") Serializable id) throws EntityNotFoundException {
		return blogService.find(id, true);
	}

	@Validated(Edit.class)
	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public Blog editBlog(@NotNull @PathVariable(name = "id") Serializable id,
			@Valid @RequestBody BlogRequest blogRequest) throws EntityNotFoundException {
		return blogService.edit(id, blogRequest.getBlog(), true);
	}

	@PostMapping(path = "/{id}/review", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public Serializable submitBlogForReview(@NotNull @PathVariable(name = "id") Serializable id)
			throws EntityNotFoundException {
		return blogService.submitBlogForReview(id);
	}
	
	@Validated(Review.class)
	@PutMapping(path = "/{id}/review", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public Serializable reviewBlog(@NotNull @PathVariable(name = "id") Serializable uuid, @Valid @RequestBody BlogReviewRequest blogReviewRequest)
			throws EntityNotFoundException {
		return blogService.reviewBlog( uuid, blogReviewRequest.getBlogReview() );
	}	

	@PostMapping(path = "/{id}/file", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Serializable uploadBlogFile(@NotNull @PathVariable(name = "id") Serializable id,
			@RequestParam(name = "file", required = true) MultipartFile file) throws IOException {
		clamRestClient.ping();
		final VirusScanResult scanResult = clamRestClient.scan(new MultipartFile[] { file });
		if (Result.VIRUS_FOUND.equals(scanResult.getResult())) {
			// TODO: throw (and handle) virus found exception
		}
		return blogService.saveBlogFile(id, file.getBytes());		
	}
}
