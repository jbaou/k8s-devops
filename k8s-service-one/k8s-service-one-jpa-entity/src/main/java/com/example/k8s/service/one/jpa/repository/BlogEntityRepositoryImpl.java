package com.example.k8s.service.one.jpa.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Service;
import com.example.k8s.service.one.jpa.entity.BlogEntity;
import com.example.k8s.common.mapper.Mapper;
import com.example.k8s.service.one.BlogRepository;
import com.example.k8s.service.one.domain.Blog;
import com.example.k8s.common.jpa.repository.MappedPagingAndSortingJpaRepositoryImpl;

@Service
public class BlogEntityRepositoryImpl extends MappedPagingAndSortingJpaRepositoryImpl<Blog, BlogEntity, Serializable> implements BlogRepository<Blog> {

	public BlogEntityRepositoryImpl(EntityManager em, Mapper<Blog, BlogEntity> mapper) {
		super(em, mapper);
	}
    
        
}
