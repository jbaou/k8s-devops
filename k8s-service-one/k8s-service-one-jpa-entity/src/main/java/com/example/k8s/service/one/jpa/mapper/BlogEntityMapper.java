package com.example.k8s.service.one.jpa.mapper;

import org.springframework.stereotype.Service;

import com.example.k8s.common.mapper.Mapper;
import com.example.k8s.service.one.domain.Blog;
import com.example.k8s.service.one.domain.Blog.Status;
import com.example.k8s.service.one.jpa.entity.BlogEntity;

@Service
public class BlogEntityMapper implements Mapper<Blog, BlogEntity> {

	@Override
	public Blog toDomain(BlogEntity e) {
		return e != null
				? Blog.builder().uuid(e.getId()).status(Status.valueOf(e.getStatus())).content(e.getContent())
						.title(e.getTitle()).build()
				: null;
	}

	@Override
	public BlogEntity toEntity(Blog d) {
		return d != null
				? BlogEntity.builder().id(d.getUuid()).status(d.getStatus().toString()).content(d.getContent())
						.title(d.getTitle()).build()
				: null;
	}

}
