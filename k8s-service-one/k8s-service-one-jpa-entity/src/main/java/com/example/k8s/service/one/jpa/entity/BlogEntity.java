package com.example.k8s.service.one.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
//@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "rxd_blog")
public class BlogEntity implements Serializable { // extends AuditbleEntity

	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "uuid", nullable = false, updatable = false, length = 255)
    private String id;	

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "content", nullable = true)
	private String content;
	
	@Column(name = "status", nullable = false)
	private String status;


}
