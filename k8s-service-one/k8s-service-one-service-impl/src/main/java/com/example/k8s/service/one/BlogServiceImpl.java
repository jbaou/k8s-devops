package com.example.k8s.service.one;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.k8s.common.exception.EntityNotFoundException;
import com.example.k8s.common.exception.RollbackException;
import com.example.k8s.common.messaging.GenericMessage;
import com.example.k8s.service.one.domain.Blog;
import com.example.k8s.service.one.domain.BlogReview;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BlogServiceImpl<D extends Blog> implements BlogService<D> {

	private @Autowired BlogRepository<D> blogRepo;
	private @Autowired MessageBrokerService messageBrockerService;	

	@Override
	@CacheEvict(value = "blogs", key = "#uuid")
	public void evict(Serializable uuid) {
		log.info("Evicting from cache blog: {}", uuid);
	}

	@Override
	@Cacheable(value = "blogs", key = "#uuid", condition = "#fromCache")
	public D find(Serializable uuid, boolean fromCache) throws EntityNotFoundException {
		// to enter the method you must set `fromCache` false
		// on method exit it will save to cache, unless `saveToCache` is false
		log.debug("fetching blogs from cache: {}", fromCache);
		return blogRepo.findById(uuid).orElseThrow(EntityNotFoundException::new);
	}

	@Override
	public List<D> findAll() {
		// do not cache entire unbounded collections ...
		return StreamSupport.stream(blogRepo.findAll().spliterator(), false).collect(Collectors.toList());
	}

	@Transactional
	@Override
	public void createAll(@NotNull List<D> l) {
		// do not cache entire unbounded collections ...
		log.info("creating blogs of size: {} ", l.size());
		l.forEach(b -> create(b, false));
	}

	@Transactional
	@Override
	@CachePut(value = "blogs", key = "#result.uuid", unless = "!#saveToCache")
	public D create(D d, boolean saveToCache) {
		log.info("creating blog: {} ", d);
		try {
			d.init();
			blogRepo.save(d);
			log.info("blog created with uuid: {} ", d.getUuid());
			// will be cached automatically by the annotation
			messageBrockerService.sendGlobalMessage(GenericMessage.builder().payload(d).build());
			return find(d.getUuid(), false);
		} catch (EntityNotFoundException e) {
			throw new RollbackException(e);
		}
	}


	@Transactional(rollbackFor = EntityNotFoundException.class)	
	@Override
	@CacheEvict(value = "blogs", key = "#uuid", beforeInvocation = true)
	@CachePut(value = "blogs", key = "#uuid", unless = "!#saveToCache")
	public D edit(Serializable uuid, D domain, boolean saveToCache) throws EntityNotFoundException {
		// it is already evicted by the annotation
		log.info("editing blog with uuid: {} ", uuid);
		D dbBlog = find(uuid, false);
		dbBlog.updateFrom(domain);
		blogRepo.save(dbBlog);
		log.info("blog edited: {} ", domain);
		// will be cached automatically by the annotation
		return find(uuid, false);
	}
	
	@Override
	@CacheEvict(value = "blogs", key = "#uuid", beforeInvocation = true)	
	public Serializable saveBlogFile(@NotNull Serializable uuid, byte[] bytes) {
		// TODO Auto-generated method stub
		return UUID.randomUUID().toString();
	}	

	@Override
	@CacheEvict(value = "blogs", key = "#uuid", beforeInvocation = true)
	public Serializable submitBlogForReview(@NotNull Serializable uuid) {
		// lets say we get a workflow id ? no idea for now ...
		return UUID.randomUUID().toString();
	}

	@Override
	public Serializable reviewBlog(@NotNull Serializable uuid, BlogReview blogReview) {
		// lets say we get a review id ? no idea for now ...
		return UUID.randomUUID().toString();
	}

}
