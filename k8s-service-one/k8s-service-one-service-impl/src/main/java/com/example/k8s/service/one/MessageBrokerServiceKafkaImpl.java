package com.example.k8s.service.one;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.example.k8s.common.messaging.Message;

import lombok.extern.slf4j.Slf4j;

@Profile("kafka")
@Slf4j
@Service
public class MessageBrokerServiceKafkaImpl implements MessageBrokerService {

	public static final String TOPIC_GLOBAL_EVENT = "global-events";

	@Autowired
	private KafkaTemplate<String, Message<?>> kafkaTemplate;
	
	public <T extends Message<?>> void sendGlobalMessage(T message) {
		log.debug("Producing message: {}", message);
		this.kafkaTemplate.send(TOPIC_GLOBAL_EVENT, message);
	}

	@KafkaListener(topics = TOPIC_GLOBAL_EVENT, groupId = "${spring.application.name}")
	public <T extends Message<?>> void consumeGlobalMessage(T message) {
		log.debug("Consumed Message: {}", message);
	}

}
