package com.example.k8s.service.one;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.k8s.common.exception.EntityNotFoundException;
import com.example.k8s.service.one.domain.Blog;
import com.example.k8s.service.one.domain.Blog.Status;

@ExtendWith(SpringExtension.class)
public class BlogServiceImplTest {

    @EnableCaching
	@AutoConfigureCache
    @TestConfiguration
    static class BlogServiceImplTestConfiguration {    	
        @Bean
        public BlogService<Blog> blogService() {
            return new BlogServiceImpl<>();
        }              
    }
    
    @Autowired private CacheManager cacheManager; 

	@Autowired
	private BlogService<Blog> blogService;

	@MockBean
	private BlogRepository<Blog> blogRepo;
	
	@MockBean
	private MessageBrokerService messageBrokerService;
	
	@BeforeEach
	public void clearCaches() {
	    for(String name : cacheManager.getCacheNames()){
	        cacheManager.getCache(name).clear(); 
	    }
	}

	@Test
	public void whenAskForBlogs_thenReturn2() {
		// in mock
		Mockito.when(blogRepo.findAll())
				.thenReturn(List.of(
						Blog.builder().uuid("xxx").title("title #1").content("content").status(Status.DRAFT).build(),
						Blog.builder().uuid("yyy").title("title #2").content("content").status(Status.DRAFT).build()));

		List<Blog> result = blogService.findAll();
		assertThat(result.isEmpty(), is(false) );
		assertThat(result.size(), is(2) );
		assertThat(result.get(0).getUuid(), is("xxx") );
		assertThat(result.get(1).getUuid(), is("yyy") );

	}
	
	@Test
	public void whenAskForExistingBlog_thenReturn1_and_testCaching() throws EntityNotFoundException {
		// in mock
		Mockito.when(blogRepo.findById("xxx"))
				.thenReturn( Optional.of(Blog.builder().uuid("xxx").title("title #1").content("content").status(Status.DRAFT).build()));
		
		Mockito.when(blogRepo.findById("yyy"))
		.thenReturn(Optional.empty());
		// test throw EntityNotFoundException
		assertThrows(EntityNotFoundException.class, ()->blogService.find("yyy", false));
		
		//test return one
		Blog result1 = blogService.find("xxx", true);
		assertThat(result1.getUuid(), is("xxx") );
		Blog result2 = blogService.find("xxx", true);
		assertThat(result1.equals(result2), is(true));
		// test caching -> mock repo should have been called only once!
	    Mockito.verify(blogRepo, Mockito.times(1)).findById("xxx");
	    // test skipping cache
		Blog result3 = blogService.find("xxx", false);
		assertThat(result2.equals(result3), is(true));
		Mockito.verify(blogRepo, Mockito.times(2)).findById("xxx");
		// test evicting
		blogService.evict("xxx");
		Blog result4 = blogService.find("xxx", false);
		assertThat(result3.equals(result4), is(true));
		Mockito.verify(blogRepo, Mockito.times(3)).findById("xxx");		
	}	
}
