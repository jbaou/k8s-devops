package com.example.k8s.service.one;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.util.List;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.k8s.common.messaging.GenericMessage;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.salesforce.kafka.test.junit5.SharedKafkaTestResource;

@ActiveProfiles("kafka")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(initializers = MessageBrokerServiceKafkaImplTest.Initializer.class, classes = {
		KafkaAutoConfiguration.class, JacksonAutoConfiguration.class, MessageBrokerServiceKafkaImplTest.Configuration.class })
public class MessageBrokerServiceKafkaImplTest {

	/**
	 * https://github.com/salesforce/kafka-junit/tree/master/kafka-junit5
	 */
	@RegisterExtension
	public static final SharedKafkaTestResource sharedKafkaTestResource = new SharedKafkaTestResource()
			.withBrokerProperty("auto.create.topics.enable", "true").withBrokerProperty("message.max.bytes", "512000");

	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
			TestPropertyValues.of(
					"spring.kafka.consumer.bootstrap-servers=" + sharedKafkaTestResource.getKafkaConnectString(),
					"spring.kafka.consumer.key-deserializer: org.apache.kafka.common.serialization.StringDeserializer",
					"spring.kafka.consumer.value-deserializer: org.springframework.kafka.support.serializer.JsonDeserializer",
					"spring.kafka.producer.bootstrap-servers=" + sharedKafkaTestResource.getKafkaConnectString(),
					"spring.kafka.producer.key-serializer: org.apache.kafka.common.serialization.StringSerializer",
					"spring.kafka.producer.value-serializer: org.springframework.kafka.support.serializer.JsonSerializer")
					.applyTo(configurableApplicationContext.getEnvironment());
		}
	}

	@Lazy(false)
	@EnableKafka	
	@TestConfiguration
	static class Configuration {
		@Bean
		public MessageBrokerService messageBrokerService() {
			return new MessageBrokerServiceKafkaImpl();
		}
	}

	@Autowired
	MessageBrokerService messageBrokerService;
	
	private final ObjectMapper mapper = new ObjectMapper();

	@BeforeAll
	static void createTopic() {
		sharedKafkaTestResource.getKafkaTestUtils().createTopic(MessageBrokerServiceKafkaImpl.TOPIC_GLOBAL_EVENT, 1,
				(short) 1);
	}

	@Test
	void sendMessageConsumeString() throws JsonParseException, JsonMappingException, IOException {
		messageBrokerService.sendGlobalMessage(GenericMessage.builder().id("xxx").payload("test xxx").build());
		messageBrokerService.sendGlobalMessage(GenericMessage.builder().id("yyy").payload("test yyy").build());
		
		final List<ConsumerRecord<byte[], byte[]>> records = sharedKafkaTestResource.getKafkaTestUtils()
				.consumeAllRecordsFromTopic(MessageBrokerServiceKafkaImpl.TOPIC_GLOBAL_EVENT);
		
		assertThat(records.size(), is(2));	
		assertThat(mapper.readValue(records.get(0).value(), new TypeReference<GenericMessage<String>>(){}).getPayload(), is("test xxx"));
		assertThat(mapper.readValue(records.get(1).value(), new TypeReference<GenericMessage<String>>(){}).getPayload(), is("test yyy"));

	}

}
