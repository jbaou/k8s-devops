package com.example.k8s.service.one.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.ManagementCenterConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@Configuration
@ConditionalOnProperty(name = "spring.caching.enabled", havingValue = "true")
@EnableCaching
public class CacheConfig {

	@Value("${spring.application.name}")
	private String appName;

	@Value("${spring.hazelcast.management.url}")
	private String hazelactManagementUrl;
	
	@Value("${spring.hazelcast.management.enabled:false}")
	private Boolean hazelactManagementEnabled;		
		
	/*
	 * HAZELCAST
	 */

	@ConditionalOnProperty(name = "spring.caching.provider", havingValue = "hazelcast")
	@Bean
	public CacheManager cacheManager(HazelcastInstance hazelcastInstance) {
		return new HazelcastCacheManager(hazelcastInstance);
	}
	
	/**
	 * 
	 * @return
	 */
	@ConditionalOnProperty(name = "spring.hazelcast.discovery.multicast.enabled", havingValue = "true")
	@Bean
	public Config config() {
		Config config = new Config(appName);
		config.getGroupConfig().setName(appName);
		config.setManagementCenterConfig(new ManagementCenterConfig().setUrl(hazelactManagementUrl).setEnabled(hazelactManagementEnabled)); // todo: from property
		return config;
	}
//	
//	@Bean
//	public HazelcastInstance instance(Config config) {
//		return Hazelcast.newHazelcastInstance(config);
//	}

}
