package com.example.k8s.service.one.config.kafla;

import java.util.List;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "kafka")
public class KafkaTopicConfigurationProperties {
	
	private List<TopicConfiguration> topics;
	
	@Getter
	@Setter
	public static class TopicConfiguration {
		String name;
		Integer numPartitions = 3;
		Short replicationFactor = 1;

		public NewTopic toNewTopic() {
			return new NewTopic(this.name, this.numPartitions, this.replicationFactor);
		}
	}
}