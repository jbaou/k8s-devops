package com.example.k8s.service.one.config;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.web.context.support.GenericWebApplicationContext;

import com.example.k8s.service.one.config.kafla.KafkaTopicConfigurationProperties;
import com.example.k8s.service.one.config.kafla.KafkaTopicConfigurationProperties.TopicConfiguration;

@Profile("kafka")
@EnableKafka
@Configuration
@Lazy(false)
public class KafkaTopicConfig {
	
	@Autowired KafkaTopicConfigurationProperties configurations;
	@Autowired GenericWebApplicationContext context;

    @Bean
    public KafkaAdmin admin(KafkaProperties properties){
        KafkaAdmin admin = new KafkaAdmin(properties.buildAdminProperties());
        admin.setFatalIfBrokerNotAvailable(true);
        return admin;
    }	

	/**
	 * initialize beans of type <code>NewTopic</code>
	 */
	@PostConstruct
	public void createTopics() {
		Optional.of(configurations.getTopics()).ifPresent(this::initializeBeans);
	}

	/**
	 * intialize beans of type <code>NewTopic</code> with name as the topic name
	 * 
	 * @param topics
	 */
	private void initializeBeans(List<TopicConfiguration> topics) {
		topics.forEach(t -> {
			context.registerBean(t.getName(), NewTopic.class, t::toNewTopic);
		});
	}
}
