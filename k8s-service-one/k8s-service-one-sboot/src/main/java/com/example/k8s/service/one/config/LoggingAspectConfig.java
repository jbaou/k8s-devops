package com.example.k8s.service.one.config;

import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import com.example.k8s.service.one.aspects.LoggingAspect;

@Configuration
@EnableAspectJAutoProxy
public class LoggingAspectConfig {

    @Bean
    public LoggingAspect loggingAspect(Environment env) {
        return new LoggingAspect(env);
    }
}
