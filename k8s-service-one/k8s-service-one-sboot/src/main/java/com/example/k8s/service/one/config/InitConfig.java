package com.example.k8s.service.one.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import com.example.k8s.service.one.BlogService;
import com.example.k8s.service.one.domain.Blog;
import com.example.k8s.service.one.domain.Blog.Status;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ConditionalOnProperty(name = "app.demo-data.enabled", havingValue = "true")
@Configuration
public class InitConfig {

	@Autowired
	BlogService<Blog> blogService;
	


	@EventListener
	public void insertBlogs(ApplicationReadyEvent event) {
		log.info("Started - creating a few blog posts");
		blogService.createAll(
			List.of(
				Blog.builder()
				.title("title #1")
				.content("content").status(Status.DRAFT).build(),
						Blog.builder()
				.title("title #2")
				.content("content").status(Status.DRAFT).build(),
						Blog.builder()
				.title("title #3")
				.content("content").status(Status.DRAFT).build(),
						Blog.builder()
				.title("title #4")
				.content("content").status(Status.DRAFT).build(),
						Blog.builder()
				.title("title #5")
				.content("content").status(Status.DRAFT).build()
		));
	}
}
